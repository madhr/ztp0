package com.sprhib.service;

import java.util.List;

import com.sprhib.dao.AlbumDAO;
import com.sprhib.model.Album;
import com.sprhib.model.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.dao.SongDAO;

@Service
@Transactional
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumDAO albumDAO;

    public void addAlbum(Album album) {
        albumDAO.addAlbum(album);
    }

    public void updateAlbum(Album album) {
        albumDAO.updateAlbum(album);
    }

    public Album getAlbum(int id) {
        return albumDAO.getAlbum(id);
    }

    public void deleteAlbum(int id) {
        albumDAO.deleteAlbum(id);
    }

    public List<Album> getAlbums() {
        return albumDAO.getAlbums();
    }

}
