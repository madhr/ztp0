package com.sprhib.service;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprhib.dao.SongDAO;

@Service
@Transactional
public class SongServiceImpl implements SongService {
	
	@Autowired
	private SongDAO songDAO;

	public void addSong(Song song) {
		songDAO.addSong(song);
	}

	public void updateSong(Song song) {
		songDAO.updateSong(song);
	}

	public Song getSong(int id) {
		return songDAO.getSong(id);
	}

	public void deleteSong(int id) {
		songDAO.deleteSong(id);
	}

	public List<Song> getSongs() {
		return songDAO.getSongs();
	}

	public List<Song> getSongsWithAlbumID(int id) { return songDAO.getSongsWithAlbumID(id); }

}
