package com.sprhib.service;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;

public interface AlbumService {

    public void addAlbum(Album album);
    public void updateAlbum(Album album);
    public Album getAlbum(int id);
    public void deleteAlbum(int id);
    public List<Album> getAlbums();

}
