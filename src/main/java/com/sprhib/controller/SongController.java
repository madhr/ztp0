package com.sprhib.controller;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;
import com.sprhib.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.service.SongService;

@Controller
@RequestMapping(value="/song")
public class SongController {
	
	@Autowired
	private SongService songService;

	@Autowired
	private AlbumService albumService;
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public ModelAndView addSongPage() {
		ModelAndView modelAndView = new ModelAndView("add-song-form");
		modelAndView.addObject("song", new Song());
		List<Album> albums = albumService.getAlbums();
		modelAndView.addObject("albums", albums);
		return modelAndView;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ModelAndView addingSong(@ModelAttribute Song song) {
		
		ModelAndView modelAndView = new ModelAndView("home");
		songService.addSong(song);
		
		String message = "Song was successfully added.";
		modelAndView.addObject("message", message);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/list")
	public ModelAndView listOfSongs() {
		ModelAndView modelAndView = new ModelAndView("list-of-songs");
		
		List<Song> songs = songService.getSongs();
		modelAndView.addObject("songs", songs);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editSongPage(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("edit-song-form");
		Song song = songService.getSong(id);
		modelAndView.addObject("song", song);
		List<Album> albums = albumService.getAlbums();
		modelAndView.addObject("albums", albums);
		return modelAndView;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edditingSong(@ModelAttribute Song song, @PathVariable Integer id) {
		
		ModelAndView modelAndView = new ModelAndView("home");
		
		songService.updateSong(song);
		
		String message = "Song was successfully edited.";
		modelAndView.addObject("message", message);
		
		return modelAndView;
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView deleteSong(@PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("home");
		songService.deleteSong(id);
		String message = "Song was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

}
