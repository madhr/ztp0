package com.sprhib.controller;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;
import com.sprhib.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sprhib.service.SongService;

@Controller
@RequestMapping(value="/album")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private SongService songService;

    @RequestMapping(value="/add", method=RequestMethod.GET)
    public ModelAndView addAlbumPage() {
        ModelAndView modelAndView = new ModelAndView("add-album-form");
        modelAndView.addObject("album", new Album());
        return modelAndView;
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)
    public ModelAndView addingAlbum(@ModelAttribute Album album) {

        ModelAndView modelAndView = new ModelAndView("home");
        albumService.addAlbum(album);

        String message = "Album was successfully added.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/list")
    public ModelAndView listOfAlbums() {
        ModelAndView modelAndView = new ModelAndView("list-of-albums");

        List<Album> albums = albumService.getAlbums();
        modelAndView.addObject("albums", albums);

        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editAlbumPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("edit-album-form");
        Album album = albumService.getAlbum(id);
        modelAndView.addObject("album", album);
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edditingAlbum(@ModelAttribute Album album, @PathVariable Integer id) {

        ModelAndView modelAndView = new ModelAndView("home");

        albumService.updateAlbum(album);

        String message = "Album was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView deleteAlbum(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("home");
        albumService.deleteAlbum(id);
        String message = "Album was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

    //TUTAJ WROCIC
    @RequestMapping(value="/details/{id}", method=RequestMethod.GET)
    public ModelAndView seeAlbumDetails(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("album-details");
        Album album = albumService.getAlbum(id);
        modelAndView.addObject("thisalbum", album);
        List<Song> songs = songService.getSongsWithAlbumID(id);
        modelAndView.addObject("songs", songs);
        return modelAndView;
    }

}
