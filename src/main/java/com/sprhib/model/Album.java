package com.sprhib.model;

import javax.persistence.*;
import java.util.Set;

// EDITED TO ADD ALBUMS
@Entity
@Table(name="albums")
public class Album {
    @Id
    @GeneratedValue
    public Integer id;

    private String albumtitle;

    @OneToMany(mappedBy="album", cascade=CascadeType.ALL)
    private Set<Song> songs;

    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}
    public String getAlbumtitle() {return albumtitle;}
    public void setAlbumtitle(String albumtitle) {this.albumtitle = albumtitle;}
    public Set<Song> getSongs() {return songs;}
    public void setSongs(Set<Song> songs) {this.songs = songs;}
}