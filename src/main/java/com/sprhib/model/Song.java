package com.sprhib.model;

import javax.persistence.*;

// EDITED TO ADD ALBUMS
@Entity
@Table(name="songs")
public class Song {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private String songtitle;
	private String artist;

	@ManyToOne
	@JoinColumn(name = "album_id")
	private Album album;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSongtitle() {
		return songtitle;
	}
	public void setSongtitle(String songtitle) {
		this.songtitle = songtitle;
	}
	public String getArtist() {return artist;}
	public void setArtist(String artist) {this.artist = artist;}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}

}
