package com.sprhib.dao;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AlbumDAOImpl implements AlbumDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void addAlbum(Album album) {
        getCurrentSession().save(album);
    }

    public void updateAlbum(Album album) {
        Album albumToUpdate = getAlbum(album.getId());
        albumToUpdate.setAlbumtitle(album.getAlbumtitle());
        getCurrentSession().update(albumToUpdate);
    }

    public Album getAlbum(int id) {
        Album album = (Album) getCurrentSession().get(Album.class, id);
        return album;
    }

    public void deleteAlbum(int id) {
        Album album = getAlbum(id);
        if (album != null)
            getCurrentSession().delete(album);
    }

    @SuppressWarnings("unchecked")
    public List<Album> getAlbums() {
        return getCurrentSession().createQuery("from Album").list();
    }

}
