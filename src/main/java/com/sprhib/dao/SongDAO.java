package com.sprhib.dao;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;

public interface SongDAO {
	
	public void addSong(Song song);
	public void updateSong(Song song);
	public Song getSong(int id);
	public void deleteSong(int id);
	public List<Song> getSongs();
	public List<Song> getSongsWithAlbumID(int id);

}
