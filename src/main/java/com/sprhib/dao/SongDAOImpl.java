package com.sprhib.dao;

import java.util.List;

import com.sprhib.model.Album;
import com.sprhib.model.Song;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SongDAOImpl implements SongDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void addSong(Song song) {
		getCurrentSession().save(song);
	}

	public void updateSong(Song song) {
		Song songToUpdate = getSong(song.getId());
		songToUpdate.setSongtitle(song.getSongtitle());
		songToUpdate.setAlbum(song.getAlbum());
		songToUpdate.setArtist(song.getArtist());
		getCurrentSession().update(songToUpdate);
		
	}

	public Song getSong(int id) {
		Song song = (Song) getCurrentSession().get(Song.class, id);
		return song;
	}

	public void deleteSong(int id) {
		Song song = getSong(id);
		if (song != null)
			getCurrentSession().delete(song);
	}

	@SuppressWarnings("unchecked")
	public List<Song> getSongs() {
		return getCurrentSession().createQuery("from Song").list();
	}

	@SuppressWarnings("unchecked")
	public List<Song> getSongsWithAlbumID(int id){
		return getCurrentSession().createCriteria(Song.class).add(Restrictions.eq("album.id",id)).list();
	}

}
