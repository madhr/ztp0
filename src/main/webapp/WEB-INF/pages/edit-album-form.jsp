<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <title>Edit album page</title>
</head>
<body>
<div class="row" align="center">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
<h1>Edit album page</h1>
<p>Here you can edit the existing album.</p>
<p>${message}</p>
<form:form role="form" method="POST" commandName="album" action="${pageContext.request.contextPath}/album/edit/${album.id}.html">
    <div class="form-group">
        <label for="albumtitle">Album title:</label>
        <form:input path="albumtitle" class="form-control" id="albumtitle" />
    </div>

    <button type="submit" class="btn btn-default">Edit</button><br/><br/><br/><br/>

</form:form>

        <p><a href="${pageContext.request.contextPath}/index.html">Home page</a></p>

    </div>
    <div class="col-sm-4"></div>
</div>
</body>
</html>