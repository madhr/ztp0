<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Home page</title>
</head>
<body>

<div class="row" align="center">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <h2>MVC, DAO, CRUD</h2>
        <p>${message}</p>
        <a href="${pageContext.request.contextPath}/song/add.html" class="btn btn-default" role="button">Add new song</a><br/><br/>
        <a href="${pageContext.request.contextPath}/song/list.html" class="btn btn-default" role="button">Song list</a><br/><br/>
        <a href="${pageContext.request.contextPath}/album/add.html" class="btn btn-default" role="button">Add new album</a><br/><br/>
        <a href="${pageContext.request.contextPath}/album/list.html" class="btn btn-default" role="button">Album list</a><br/>
    </div>
    <div class="col-sm-4"></div>
</div>


</body>
</html>