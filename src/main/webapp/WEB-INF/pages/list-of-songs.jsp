<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
		  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>List of songs</title>
</head>
<body>
<div class="row" align="center">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
<h1>List of songs</h1>
<p>You can edit or delete list items.</p>
<table class="table-bordered table-striped" >
<thead>
<tr>
<th class="text-center" width="5%">id</th><th class="text-center" width="20%">Song title</th><th class="text-center" width="25%">Album</th><th class="text-center" width="20%">Artist</th><th class="text-center" width="27%">Actions</th>
</tr>
</thead>
<tbody class="text-center">
<c:forEach var="song" items="${songs}">
<tr>
	<td>${song.id}</td>
	<td>${song.songtitle}</td>
	<td>${song.album.albumtitle}</td>
	<td>${song.artist}</td>
	<td>
	<a href="${pageContext.request.contextPath}/song/edit/${song.id}.html" class="btn btn-default" role="button">Edit</a>
	<a href="${pageContext.request.contextPath}/song/delete/${song.id}.html" class="btn btn-default" role="button">Delete</a>
	</td>
</tr>
</c:forEach>
</tbody>
</table>

<p><a href="${pageContext.request.contextPath}/index.html">Home page</a></p>
	</div>
	<div class="col-sm-2"></div>
</div>

</body>
</html>