<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
		  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>Add song page</title>
</head>
<body>
<div class="row" align="center">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<h2>Add a song page</h2>
		<p>Here you can add a new song.</p>
		<form:form role="form" method="POST" commandName="song" action="${pageContext.request.contextPath}/song/add.html">
			<div class="form-group">
				<label for="songtitle">Song title:</label>
				<form:input path="songtitle" class="form-control" id="songtitle" />
			</div>
			<div class="form-group">
				<label for="songtitle">Album title:</label>
				<form:select path="album.id" items="${albums}" itemValue="id" itemLabel="albumtitle" class="form-control" id="albumtitle" />
			</div>


			<div class="form-group">
				<label for="artist">Artist:</label>
				<form:input path="artist" class="form-control" id="artist" />
			</div>

			<button type="submit" class="btn btn-default">Add</button><br/><br/><br/><br/>

		</form:form>

		<p><a href="${pageContext.request.contextPath}/index.html">Home page</a></p>
	</div>
	<div class="col-sm-4"></div>
</div>
</body>
</html>